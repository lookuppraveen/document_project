import 'index.scss';
import App from 'app/app';
import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from 'reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
// import CheckoutForm from '../checkoutform';
import { loadStripe } from '@stripe/stripe-js';
import {
    Elements,
} from '@stripe/react-stripe-js';

const stripePromise = loadStripe('pk_test_51KIJcGBGfTZXz1kcUGJ7DrWgFuG6t4oAri8wfNXudAz7CdXnfJN25ycxpAXgr5gdyKRQNMPU6nqZJVLnfwdFqtPv00yZkiVqYK');
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <Elements stripe={stripePromise} >
    <App />
 </Elements>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
